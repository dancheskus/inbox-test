import React, { useState } from 'react';
import { connect } from 'react-redux';

import CircleButton from './styled-components/CircleButton';
import { removeEmployee } from '../redux/actions/appActions';
import { Record, Section, Sections } from './styled-components/TableRecordStyles';

const cutString = str => (str.length > 13 ? str.substring(0, 9) + '...' : str);

const TableRecord = ({ data, removeEmployee }) => {
  const { id, employee_name, employee_salary, employee_age } = data;

  const [isRemoving, setIsRemoving] = useState(false);

  return (
    <Record>
      {data && (
        <Sections>
          <Section>{id}</Section>
          <Section>{cutString(employee_name)}</Section>
          <Section>{cutString(employee_salary)}</Section>
          <Section>{cutString(employee_age)}</Section>
          <Section>
            <CircleButton
              onClick={() => {
                setIsRemoving(true);
                removeEmployee(id);
              }}
              remove
              isRemoving={isRemoving}
            >
              &mdash;
            </CircleButton>
          </Section>
        </Sections>
      )}
    </Record>
  );
};

export default connect(
  null,
  { removeEmployee }
)(TableRecord);
