import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { connect } from 'react-redux';

import NewEmployeeForm from './NewEmployeeForm';
import CircleButton from './styled-components/CircleButton';
import { addEmployee } from '../redux/actions/appActions';

const AddRecordModal = ({ addEmployee }) => {
  const [modal, setModal] = useState(false);
  const [responseModal, setResponseModal] = useState(false);
  const [responseModalText, setResponseModalText] = useState('');

  const toggle = () => setModal(!modal);
  const responseToggle = () => setResponseModal(!responseModal);

  return (
    <>
      <CircleButton onClick={toggle}>+</CircleButton>

      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Add employee</ModalHeader>

        <ModalBody>
          <NewEmployeeForm onSubmit={data => addEmployee(data, setResponseModal, setResponseModalText, setModal)} />
        </ModalBody>
      </Modal>

      <Modal isOpen={responseModal} toggle={responseToggle}>
        <ModalHeader toggle={responseToggle}>Response</ModalHeader>

        <ModalBody>{responseModalText}</ModalBody>

        <ModalFooter>
          <Button color='primary' onClick={responseToggle}>
            OK
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default connect(
  null,
  { addEmployee }
)(AddRecordModal);
