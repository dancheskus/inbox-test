import React, { useState } from 'react';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import styled from 'styled-components';
import { Row } from 'reactstrap';

import TableRecord from './TableRecord';
import AddRecordModal from './AddRecordModal';
import { Table, Columns, Column } from './styled-components/TableStyles';

const Wrapper = styled(Row)`
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const TableComponent = ({ employees }) => {
  const [currentPage, setCurrentPage] = useState(0);

  return (
    <Wrapper>
      <AddRecordModal />

      <Table>
        <Columns>
          <Column>id</Column>
          <Column>name</Column>
          <Column>salary</Column>
          <Column>age</Column>
          <Column>remove</Column>
        </Columns>

        {Object.keys(employees)
          .slice(currentPage * 10, currentPage * 10 + 10)
          .map(id => (
            <TableRecord key={id} data={{ id, ...employees[id] }} />
          ))}

        <ReactPaginate
          pageCount={Math.ceil(Object.keys(employees).length / 10)}
          pageRangeDisplayed={3}
          marginPagesDisplayed={2}
          containerClassName='pagesContainer'
          previousLabel='&larr;'
          nextLabel='&rarr;'
          activeClassName='activePage'
          onPageChange={({ selected }) => setCurrentPage(selected)}
        />
      </Table>
    </Wrapper>
  );
};

export default connect(({ employees }) => ({ employees }))(TableComponent);
