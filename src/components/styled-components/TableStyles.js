import styled from 'styled-components';

export const Table = styled.div`
  width: 600px;
  box-shadow: 4px 4px 41px -20px rgba(0, 0, 0, 0.75);
  padding: 20px;
  display: flex;
  flex-direction: column;

  .pagesContainer {
    display: flex;
    align-self: center;
    margin-top: 10px;
    max-width: 590px;

    li {
      width: 40px;
      height: 40px;
      list-style: none;
      border: 1px solid #168bf2;
      color: #168bf2;
      display: flex;
      justify-content: center;
      align-items: center;
      border-radius: 50%;

      a {
        outline: none;
        cursor: pointer;

        :hover {
          text-decoration: none;
        }
      }
      :not(:last-child) {
        margin-right: 10px;
      }
    }

    .activePage {
      background-color: #168bf2;
      a {
        color: white;
      }
    }
  }
`;

export const Columns = styled.div`
  height: 30px;
  display: flex;
  border-bottom: 1px solid lightgrey;
  margin-bottom: 20px;
`;

export const Column = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  text-transform: uppercase;
`;
