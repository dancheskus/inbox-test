import styled from 'styled-components';

export default styled.div`
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  box-shadow: 10px 10px 34px -17px rgba(0, 0, 0, 0.75);
  transition: all 0.3s;
  filter: brightness(100%);
  cursor: pointer;

  ${({ remove, isRemoving }) => {
    if (remove) {
      return `
        background-color: ${isRemoving ? 'grey' : 'orangered'};
        width: 20px;
        height: 20px;
        font-size: 60%;

        :hover {
          filter: brightness(130%);
        }
      `;
    }

    return `
      background-color: #168bf2;
      width: 50px;
      height: 50px;
      font-size: 250%;
      font-weight: 500;
      margin-bottom: 20px;


      :hover {
        transform: translateY(-2px);
      }
    `;
  }};
`;
