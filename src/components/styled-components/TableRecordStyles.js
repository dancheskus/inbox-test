import styled from 'styled-components';

export const Record = styled.div`
  width: 100%;
  padding: 10px 0;

  :not(:last-child) {
    border-bottom: 1px solid black;
  }
`;

export const Sections = styled.div`
  display: flex;
`;

export const Section = styled.div`
  width: 20%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
