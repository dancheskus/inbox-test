import React from 'react';
import { FormGroup, Label, Input } from 'reactstrap';
import { Field } from 'redux-form';

import _ from 'lodash';

const renderInput = props => <Input {...props.input} type={props.type} required />;

export default ({ name, type }) => (
  <FormGroup>
    <Label for={name}>{_.capitalize(name)}</Label>
    <Field name={name} component={renderInput} type={type} />
  </FormGroup>
);
