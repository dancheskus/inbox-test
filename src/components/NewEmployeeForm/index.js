import React from 'react';
import { reduxForm } from 'redux-form';
import { Button, Form } from 'reactstrap';
import FormGroup from './FormGroup';

const NewEmployeeForm = ({ handleSubmit }) => (
  <Form onSubmit={handleSubmit}>
    <FormGroup name='name' type='text' />
    <FormGroup name='salary' type='number' />
    <FormGroup name='age' type='number' />

    <Button type='submit'>Submit</Button>
  </Form>
);

export default reduxForm({ form: 'newEmployeeForm' })(NewEmployeeForm);
