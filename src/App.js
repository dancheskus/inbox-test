import React from 'react';
import Table from './components/Table';
import { connect } from 'react-redux';
import { BounceLoader } from 'react-spinners';
import styled from 'styled-components';

const Wrapper = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const App = ({ employees }) => {
  if (employees.error)
    return (
      <Wrapper>
        <h1>{employees.error}</h1>
      </Wrapper>
    );

  return Object.keys(employees).length ? (
    <Wrapper>
      <Table />
    </Wrapper>
  ) : (
    <Wrapper>
      <BounceLoader sizeUnit={'px'} size={50} color={'#168bf2'} />
    </Wrapper>
  );
};

export default connect(({ employees }) => ({ employees }))(App);
