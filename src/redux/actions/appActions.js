import axios from 'axios';

export const fetchEmployees = () => dispatch =>
  axios
    .get(process.env.REACT_APP_URL + 'employees')
    .then(({ data }) => dispatch({ type: 'FETCH_EMPLOYEES', data }))
    .catch(error => dispatch({ type: 'SERVER_ERROR' }));

export const removeEmployee = id => dispatch => {
  axios
    .delete(process.env.REACT_APP_URL + 'delete/' + id)
    .then(() => dispatch({ type: 'REMOVE_EMPLOYEE', id }))
    .catch(err => console.error(err));
};

export const addEmployee = (data, setResponseModal, setResponseModalText, setModal) => dispatch => {
  axios
    .post(process.env.REACT_APP_URL + 'create', { ...data })
    .then(({ data: { id, name, salary, age } }) => {
      if (!id) {
        setResponseModalText('Employee name already exists!');
      } else {
        dispatch({
          type: 'ADD_EMPLOYEE',
          data: {
            id,
            employee_name: name,
            employee_salary: salary,
            employee_age: age,
          },
        });
        setResponseModalText('Employee created!');
      }
      setModal(false);
      setResponseModal(true);
    })
    .catch(err => {
      console.error(err);
      setModal(false);
    });
};
