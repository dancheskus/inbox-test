/* eslint-disable */

import produce from 'immer';
import _ from 'lodash';

export default (state = {}, action) =>
  produce(state, draft => {
    switch (action.type) {
      case 'FETCH_EMPLOYEES':
        action.data.forEach(el => (draft[el.id] = _.omit(el, ['id'])));
        break;

      case 'REMOVE_EMPLOYEE':
        delete draft[action.id];
        break;

      case 'ADD_EMPLOYEE':
        draft[action.data.id] = _.omit(action.data, ['id']);
        break;

      case 'SERVER_ERROR':
        draft.error = 'Server is not working.';
        break;
    }
  });
