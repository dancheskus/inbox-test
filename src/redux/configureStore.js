import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';

import appReducer from './reducers/appReducer';

export default () =>
  createStore(
    combineReducers({ employees: appReducer, form: formReducer }),
    composeWithDevTools(applyMiddleware(thunk))
  );
